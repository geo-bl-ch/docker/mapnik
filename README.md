This project is used to build [mapnik](https://github.com/mapnik/mapnik)

It has no other purpose. So it offers the built images for dedicated versions.

All credits go to the maintainers of the mapnik project for their great work.

Each image tag belongs to one branch in this repo. So master branch is only used for documentation.

| branch | image (to use in a Dockerfile) |
| ---  |  ---  |
|  [3.0.12](https://gitlab.com/geo-bl-ch/docker/mapnik/-/tree/3.0.12)  |  `registry.gitlab.com/geo-bl-ch/docker/mapnik:3.0.12`  |
|  [3.0.23](https://gitlab.com/geo-bl-ch/docker/mapnik/-/tree/3.0.23)  |  `registry.gitlab.com/geo-bl-ch/docker/mapnik:3.0.23`  |

Please refer to the [registry of this project](https://gitlab.com/geo-bl-ch/docker/mapnik/container_registry/) to see images in browsable view:
